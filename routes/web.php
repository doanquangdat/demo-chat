<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', function () {
    return view('chat');
})->middleware('auth');

Route::get('/getUserLogin', function() {
    return \Illuminate\Support\Facades\Auth::user();
})->middleware('auth');

Route::get('/messages', function() {
    return \App\Message::with('user')->get();
})->middleware('auth');

Route::post('/messages', function() {
    $user = \Illuminate\Support\Facades\Auth::user();
    $message = new \App\Message();
    $message->message = request()->get('message', '');
    $message->user_id = $user->id;
    $message->save();
    broadcast(new App\Events\MessagePosted($message, $user))->toOthers();
    return ['message' => $message->load('user')];
})->middleware('auth');


//Route::get('/home', 'HomeController@index')->name('home');
